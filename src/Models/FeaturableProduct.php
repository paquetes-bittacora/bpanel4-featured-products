<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\FeaturedProducts\Models;

use Bittacora\Bpanel4\Products\Models\Product;
use Bittacora\Content\Models\ContentModel;
use Illuminate\Database\Eloquent\Relations\MorphOne;

/**
 * Este modelo solo se usa internamente, no es un modelo aparte de Product y todas sus propiedades están en la tabla
 * products. Lo único que hace es exponer un par de funciones para mejorar el tipado.
 *
 * @property int|bool $featured
 * @method static FeaturableProduct whereId($value)
 * @method FeaturableProduct firstOrFail()
 */
final class FeaturableProduct extends Product
{
    public $table = 'products';

    public function getMorphClass(): string
    {
        return Product::class;
    }

    public function setFeatured(bool $featured): void
    {
        $this->featured = $featured;
    }

    public function isFeatured(): bool
    {
        return (bool)$this->featured;
    }
}
