<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\FeaturedProducts\Http\Listeners;

use Bittacora\Bpanel4\FeaturedProducts\Models\FeaturableProduct;
use Bittacora\Bpanel4\Products\Events\ProductCreated;
use Bittacora\Bpanel4\Products\Events\ProductUpdated;

final class SaveProductFeaturedStatus
{
    public function handle(ProductCreated|ProductUpdated $event): void
    {
        $product = FeaturableProduct::whereId($event->getProduct()->getId())->firstOrFail();
        $product->setFeatured($this->productIsFeatured($event));
        $product->save();
    }

    public function productIsFeatured(ProductCreated|ProductUpdated $event): bool
    {
        return "1" === $event->getRequest()->get('featured');
    }
}
