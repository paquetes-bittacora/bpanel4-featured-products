<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\FeaturedProducts\Http\Controllers;

use Bittacora\Bpanel4\Products\Http\Controllers\ProductAdminController;
use Bittacora\Bpanel4\Products\Models\Product;
use Illuminate\View\View;
use RuntimeException;

final class FeaturedProductsAdminController extends ProductAdminController
{
    public function create(?View $view = null): View
    {
        if (null !== $this->decorated) {
            $view = $this->decorated->create($view);
        }

        if (!$view instanceof View) {
            throw new RuntimeException();
        }

        return $view->nest(
            'before-active-additional-fields',
            'bpanel4-featured-products::bpanel.create',
            $view->getData()
        );
    }

    public function edit(Product $model, string $locale = 'es', ?View $view = null): View
    {
        if (null !== $this->decorated) {
            $view = $this->decorated->edit($model, $locale, $view);
        }

        $this->checkDecoratorValidity($view);

        return $view->nest(
            'before-active-additional-fields',
            'bpanel4-featured-products::bpanel.edit',
            $view->getData()
        );
    }
}
