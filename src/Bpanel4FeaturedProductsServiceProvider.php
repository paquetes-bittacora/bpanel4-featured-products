<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\FeaturedProducts;

use Bittacora\Bpanel4\FeaturedProducts\Http\Controllers\FeaturedProductsAdminController;
use Bittacora\Bpanel4\FeaturedProducts\Http\Listeners\SaveProductFeaturedStatus;
use Bittacora\Bpanel4\Products\Events\ProductCreated;
use Bittacora\Bpanel4\Products\Events\ProductUpdated;
use Bittacora\Bpanel4\Products\Http\Controllers\ProductAdminController;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

final class Bpanel4FeaturedProductsServiceProvider extends ServiceProvider
{
    private const PACKAGE_PREFIX = 'bpanel4-featured-products';

    public function register(): void
    {
        parent::register();

        $this->app->extend(
            ProductAdminController::class,
            static function (ProductAdminController $controller, Application $app) {
                $newController = $app->make(FeaturedProductsAdminController::class);
                $newController->setDecorated($controller);
                return $newController;
            }
        );
    }

    public function boot(Dispatcher $eventsDispatcher): void
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', self::PACKAGE_PREFIX);
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', self::PACKAGE_PREFIX);
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

        $this->registerEventListeners($eventsDispatcher);
    }

    private function registerEventListeners(Dispatcher $eventsDispatcher): void
    {
        $eventsDispatcher->listen(ProductCreated::class, SaveProductFeaturedStatus::class);
        $eventsDispatcher->listen(ProductUpdated::class, SaveProductFeaturedStatus::class);
    }
}
